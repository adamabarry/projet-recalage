# Projet Recalage

Ce projet méthodologique porte sur le recalage d'images des plantes à l'aide de trois méthodes de récalage : la prémiere basée sur le **coherent point drift**, la séconde sur l'**ORB** et enfin la troisième sur l'**information mutuelle (MI)**.

Le rapport du projet et la présentation sont parmis les documents PDF. Bonne lecture !
 
